<?php

// Change your email here ↓
define('EMAIL', 'demo@demo.com');

error_reporting(0);

$name = $_POST['name'];
$email = $_POST['email'];
$in_time = isset($_POST['in-time']) ? $_POST['in-time'] : '';
$page = isset($_POST['page']) ? $_POST['page'] : '';

if (strlen($name) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $message = 'Name: ' . $name . "\n";
    $message .= 'Email: ' . $email . "\n";
    $message .= 'From page: ' . $page . "\n";
    if(isset($_POST['in-time'])){
        $message .= 'Is in time: ' . $in_time ? 'Yes' : 'No'. "\n";
    }

    mail(EMAIL, "Get Your Youth Back And Feel Confident Again!", $message,
        "From: demo@demo.com \r\n"
        ."X-Mailer: PHP/" . phpversion());

    die(json_encode(array('error' => 0, 'message' => 'Message sended!')));
} else {
    $fields = array();

    if(!strlen($name)){
        $fields[] = 'name';
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $fields[] = 'email';
    }
    die(json_encode(array(
        'error' => 1,
        'message' => 'Fill all fields',
        'fields' => $fields
    )));
}
