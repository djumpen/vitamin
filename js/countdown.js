var timer_tick_callback, timer_end_callback;

function counter(end_date){
    var global_date = end_date;
    var days = end_date / 3600 / 24; days = days - (days%1);
    var hours;
    var minutes;
    var seconds;
    if(days >= 1) end_date = end_date - (3600 * 24 * days);
    hours = end_date / 3600; hours = hours - (hours%1);
    if(hours >= 1)  end_date = end_date - (3600 * hours);
    minutes = end_date / 60; minutes = minutes - (minutes%1);
    if(minutes >= 1) end_date = end_date - (60 * minutes);
    seconds = end_date;
    //var html = LANGS.default.counter_text_prepend;
    var html = '';
    //if(days > 0) html += "<span class='days'>" + days + "</span>";
    if(days.toString().length == 1) days = "0" + days.toString();
    if(hours.toString().length == 1) hours = "0" + hours.toString();
    if(minutes.toString().length == 1) minutes = "0" + minutes.toString();
    if(seconds.toString().length == 1) seconds = "0" + seconds.toString();
    //html += "<span class='hours'>" + hours + "</span><span class='minutes'>" + minutes + "</span><span class='seconds'>" + seconds + "</span>";
    //container.html(html);
    var response = [];
    response.days = days.toString();
    response.hours = hours.toString();
    response.minutes = minutes.toString();
    response.seconds = seconds.toString();

    timer_tick_callback(response);
    setTimeout(function(){
        if(global_date > 0){
            global_date--;
            $.cookie('timer', global_date, { expires: 7 });
            counter(global_date);
        } else {
            //container.html('times up');
            $.cookie('timer', 0, { expires: 7 });
            timer_end_callback();
        }
    }, 1000);
}